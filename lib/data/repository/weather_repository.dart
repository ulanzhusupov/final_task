import 'package:dio/dio.dart';
import 'package:final_task/core/app_api.dart';
import 'package:final_task/data/dio_settings.dart';
import 'package:final_task/data/model/weather_model.dart';

class WeatherRepository {
  final DioSettings dioSettings;

  WeatherRepository({required this.dioSettings});

  Future<WeatherModel> getWeatherByLatLng() async {
    try {
      final result = await dioSettings.dio.get(
        "https://api.openweathermap.org/data/2.5/weather",
        queryParameters: {
          "lat": "42.88",
          "lon": "74.58",
          "appid": AppAPI.appApi,
          "units": "metric",
        },
      );
      return WeatherModel.fromJson(result as Map<String, dynamic>);
    } catch (e) {
      if (e is DioException) {
        throw DioException(requestOptions: e.requestOptions);
      } else {
        throw UnimplementedError(e.toString());
      }
    }
  }
}

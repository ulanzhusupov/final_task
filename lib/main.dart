import 'package:final_task/data/dio_settings.dart';
import 'package:final_task/data/repository/weather_repository.dart';
import 'package:final_task/presentation/state/cubit/weather_cubit.dart';
import 'package:final_task/presentation/theme/app_theme.dart';
import 'package:final_task/presentation/state/bloc/theme_bloc.dart';
import 'package:final_task/presentation/state/bloc/theme_event.dart';
import 'package:final_task/presentation/state/bloc/theme_state.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MultiRepositoryProvider(
      providers: [
        RepositoryProvider<DioSettings>(create: (context) => DioSettings()),
        RepositoryProvider<WeatherRepository>(
          create: (context) => WeatherRepository(
            dioSettings: RepositoryProvider.of<DioSettings>(context),
          ),
        ),
      ],
      child: MultiBlocProvider(
        providers: [
          BlocProvider(create: (context) => ThemeBloc()),
          BlocProvider(
              create: (context) => WeatherCubit(
                  weatherRepository:
                      RepositoryProvider.of<WeatherRepository>(context))),
        ],
        child: BlocBuilder<ThemeBloc, ThemeState>(
          builder: (context, state) {
            return MaterialApp(
              title: 'Flutter Demo',
              theme: state.themeData,
              home: const MyHomePage(title: 'Flutter Demo Home Page'),
            );
          },
        ),
      ),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key, required this.title});

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int _counter = 0;
  bool isDarkMode = false;
  String weatherText = "";

  void _incrementCounter() {
    setState(() {
      if (_counter < 10) {
        _counter++;
      }
    });
  }

  void _decrementCounter() {
    setState(() {
      if (_counter > 0) {
        _counter--;
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).colorScheme.inversePrimary,
        title: Text(widget.title),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            BlocListener<WeatherCubit, WeatherState>(
              listener: (context, state) {
                // if (state is WeatherLoading) {
                //   return const CircularProgressIndicator();
                // }
                if (state is WeatherError) {
                  weatherText = state.errText;
                  setState(() {});
                }
                if (state is WeatherSuccess) {
                  weatherText =
                      "Weather for ${state.weatherModel.name}\n${state.weatherModel.main?.temp}";
                  setState(() {});
                }
              },
              child: Text(weatherText),
            ),
            // builder: (context, state) {
            //   if (state is WeatherLoading) {
            //     return const CircularProgressIndicator();
            //   }
            //   if (state is WeatherError) {
            //     return Text(state.errText);
            //   }
            //   if (state is WeatherSuccess) {
            //     return Text(
            //         "Weather for ${state.weatherModel.name}\n${state.weatherModel.main?.temp}");
            //   }
            //   return const SizedBox();
            // },

            const Text(
              'You have pushed the button this many times:',
            ),
            Text(
              '$_counter',
              style: Theme.of(context).textTheme.headlineMedium,
            ),
          ],
        ),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.endFloat,
      floatingActionButton: Padding(
        padding: const EdgeInsets.only(left: 32),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Column(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                FloatingActionButton(
                  onPressed: () {
                    BlocProvider.of<WeatherCubit>(context).getWeatherByLatLng();
                  },
                  tooltip: 'Weather',
                  child: const Icon(Icons.cloud),
                ),
                const SizedBox(height: 10),
                FloatingActionButton(
                  onPressed: () {
                    if (isDarkMode) {
                      isDarkMode = false;
                      BlocProvider.of<ThemeBloc>(context)
                          .add(ChangeThemeEvent(themeData: AppTheme.lightMode));
                    } else {
                      isDarkMode = true;
                      BlocProvider.of<ThemeBloc>(context)
                          .add(ChangeThemeEvent(themeData: AppTheme.darkMode));
                    }
                  },
                  tooltip: 'Theme',
                  child: const Icon(Icons.palette),
                ),
              ],
            ),
            Column(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                FloatingActionButton(
                  onPressed: _incrementCounter,
                  tooltip: 'Increment',
                  child: const Icon(Icons.add),
                ),
                const SizedBox(height: 10),
                FloatingActionButton(
                  onPressed: _decrementCounter,
                  tooltip: 'Decrement',
                  child: const Icon(Icons.remove),
                ),
              ],
            ),
          ],
        ),
      ), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}

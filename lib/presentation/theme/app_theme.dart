import 'package:flutter/material.dart';

final class AppTheme {
  static ThemeData lightMode = ThemeData.light();
  static ThemeData darkMode = ThemeData.dark();
}

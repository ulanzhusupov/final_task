import 'package:final_task/presentation/state/bloc/theme_event.dart';
import 'package:final_task/presentation/state/bloc/theme_state.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class ThemeBloc extends Bloc<ThemeEvent, ThemeState> {
  ThemeBloc() : super(ThemeInitial()) {
    on<ChangeThemeEvent>((event, emit) {
      emit(ThemeLoading());
      try {
        emit(
          ThemeSuccess(themeData: event.themeData),
        );
      } catch (e) {
        emit(ThemeError(errText: e.toString()));
      }
    });
  }
}

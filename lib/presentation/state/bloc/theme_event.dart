import 'package:flutter/material.dart';

abstract class ThemeEvent {}

class ChangeThemeEvent extends ThemeEvent {
  ChangeThemeEvent({required this.themeData});

  final ThemeData themeData;
}

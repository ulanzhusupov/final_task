import 'package:final_task/presentation/theme/app_theme.dart';
import 'package:flutter/material.dart';

abstract class ThemeState {
  ThemeData? themeData = AppTheme.lightMode;
  ThemeState();
}

class ThemeInitial extends ThemeState {
  ThemeInitial();
}

class ThemeSuccess extends ThemeState {
  @override
  final ThemeData themeData;
  ThemeSuccess({
    required this.themeData,
  });
}

class ThemeLoading extends ThemeState {
  ThemeLoading();
}

class ThemeError extends ThemeState {
  final String errText;

  ThemeError({
    required this.errText,
  });
}

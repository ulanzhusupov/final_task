import 'package:final_task/data/model/weather_model.dart';
import 'package:final_task/data/repository/weather_repository.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

part 'weather_state.dart';

class WeatherCubit extends Cubit<WeatherState> {
  WeatherCubit({required this.weatherRepository}) : super(WeatherInitial());
  final WeatherRepository weatherRepository;

  Future<void> getWeatherByLatLng() async {
    try {
      emit(WeatherLoading());
      WeatherModel weatherModel = await weatherRepository.getWeatherByLatLng();
      emit(WeatherSuccess(weatherModel: weatherModel));
    } catch (e) {
      emit(WeatherError(errText: e.toString()));
    }
  }
}
